// Require dependencies

//GULP
const { src, dest, watch, parallel} = require("gulp"); // Here gulp means the installed gulp tool.

//CSS
const sass = require("gulp-sass")(require("sass"));
const plumber = require('gulp-plumber');
const autoprefixer = require ('autoprefixer');
const cssnano = require ('cssnano');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');

//JavaScript
const terser = require('gulp-terser-js');


// Images
const webp = require('gulp-webp');  /*As admin: npm install --save-dev gulp-webp */  /* import * as webp from 'gulp-webp'; */
const imagemin = require ('gulp-imagemin'); /* npm install --save-dev gulp-imagemin@7.1.0 */
const cache = require ('gulp-cache'); // Required for the lightImages task
const avif = require('gulp-avif');  /* npm install --save-dev gulp-avif */

function css(done) {
  src('src/scss/**/*.scss') 
    .pipe(sourcemaps.init())  // Save CSS reference in order to keep that information after optimization
    .pipe(plumber()) // Do not stop workflow everytime there is an error
    .pipe(sass())  //Compile scss file. Here sass() is equal to sass --watch src/scss:build/css from package.json
    .pipe(postcss([ autoprefixer(), cssnano() ])) // Optimize CSS generated code
    .pipe(sourcemaps.write('.')) // '.' is included so that the references are saved in the same destination directory as the CSS stylesheet
    .pipe(dest("build/css")); // Store the compiled file in build/css

    done(); // Callback function (this is executed to indicate the task is finished)
} // See ---------- Exports ---------- 

// Task: Image conversion to webp.
function webpVersion( done ) {
  const options = {
    quality: 50 // 50% quality will still make a good quality image.
  };
  
  src('src/img/**/*.{png,jpg}') // In all folders inside 'src/img', search for png and jpg files
    .pipe( webp(options))       // Convert all images found by src to webp.
    .pipe( dest('build/img'))   // Once the conversion is complete, store the new images in 'build/img'
  done();
} 

// Task: Convert Non webp images into lighter files.
function lightImages( done) {
  const options = {
    optimizationLevel: 3 // This will make the images lighter
  }
  src('src/img/**/*.{png,jpg}')     // Find all images
    .pipe(cache(imagemin(options))) // Make images lighter and save them on cache
    .pipe(dest('build/img'))        // Save the lighter images in /build/img
  done();
}

// Task: Image conversion to avif.
function avifVersion( done ) {
  const options = {
    quality: 50 // 50% quality will still make a good quality image.
  };
  
  src('src/img/**/*.{png,jpg}') // In all folders inside 'src/img', search for png and jpg files
    .pipe( avif(options))       // Convert all images found by src to webp.
    .pipe( dest('build/img'))   // Once the conversion is complete, store the new images in 'build/img'
  done();
} 

function javascript( done ) {
  src('src/js/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(terser())
    .pipe(sourcemaps.write('.'))
    .pipe(dest('build/js')) ;
  done();
}

function dev(done) {
  watch('src/scss/**/*.scss', css);    // Now this will run the watch, and everytime the scss file is modified, the css task will be executed.
  watch('src/js/**/*.js', javascript); // Watch for chancges in js files and execute javascript task as well. 
  done(); 
} // See ---------- Exports ---------- 

function build(done) {

  done();
}

// -------------------- Exports --------------------

exports.css = css; //  Use ---- npx gulp css ---- or add "css": "gulp css" to package.json and use ---- npm run css----
exports.javascript = javascript;
/* exports.dev = dev; */ // npx gulp dev or add "dev" : "gulp dev" to the scripts in package.json and use npm run dev
exports.webpVersion = webpVersion; // npx gulp webpVersion
exports.avifVersion = avifVersion; // npx gulp avifVersion
exports.lightImages = lightImages; // npx gulp lightImages
// Parallel will make it so that npx gulp dev executes several tasks. Comment "exports.dev = dev;" if you use this.
exports.dev = parallel(webpVersion, avifVersion, lightImages, javascript, css, dev);
exports.build = parallel(webpVersion, avifVersion, lightImages, javascript, css, build);