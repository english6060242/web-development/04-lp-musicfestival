document.addEventListener('DOMContentLoaded', function (){
    initApp();
});

function initApp() {
    createGalery(); // Display Galery, photos when click, undo when licking anywhere else, etc
    scrollNav();    // Smooth scrolling instead of reular navigation using anchor tags
    fixedNav();     // Detect scrolling in order to include the navigation var on top after certain amount of scrolling
}

function scrollNav() {
    const navLinks = document.querySelectorAll('.principal_nav');
    navLinks.forEach( navLink => {
        navLink.addEventListener('click', function(e) {
            e.preventDefault();
            const sectionScroll = e.target.attributes.href.value;
            const mySection = document.querySelector(sectionScroll);
            mySection.scrollIntoView({ behavior: "smooth" });
        })
    })
}

function fixedNav() {
    const navBar = document.querySelector('.header');
    const aboutFestival = document.querySelector('.about_festival');
    const body = document.querySelector('body');

    window.addEventListener('scroll', function() {
        /* console.log(aboutFestival.getBoundingClientRect()); */
        const scrollAbout_Festival = aboutFestival.getBoundingClientRect();
        // Check whether the bottom margin of the target element has toucheed the viewport (the part of the browser that displays the website).
        /* if(scrollAbout_Festival.bottom < 0){    */
        // Check whether the top margin of the target element has toucheed the viewport (the part of the browser that displays the website).
        if(scrollAbout_Festival.top < 0){       
            /* console.log("element reached"); */ // If it has, the element has been reached by scrolling
            navBar.classList.add('fixedNav');
            body.classList.add('body_scroll');
        }else {
            navBar.classList.remove('fixedNav');
            body.classList.remove('body_scroll');
        }
    })
}

function createGalery() {
    const galery = document.querySelector('.galery_images');

    for(let i = 1; i <= 12; i++) {
        const myImage = document.createElement('picture');
        myImage.innerHTML = `
            <source srcset="build/img/thumb/${i}.avif" type="image/avif">
            <source srcset="build/img/thumb/${i}.webp" type="image/webp">
            <img loading="lazy" width="200" height="300" src="src/img/thumb/${i}.jpg" alt="Galery Image">
        `;
        myImage.onclick = function () { // Use this callback format instead of onclick = displayImage(i) to prevent all images from displaying
            displayImage(i);
        }
        galery.appendChild(myImage);
    }
}

function displayImage(id) {
    const myImage = document.createElement('picture');
        myImage.innerHTML = `
            <source srcset="build/img/big/${id}.avif" type="image/avif">
            <source srcset="build/img/big/${id}.webp" type="image/webp">
            <img loading="lazy" width="200" height="300" src="src/img/big/${id}.jpg" alt="Galery Image">
        `;
    
    // Create the overlay (modal window) containing the image
    const overlay = document.createElement('div');
    overlay.appendChild(myImage);
    overlay.classList.add('overlay');
    overlay.onclick = function() {
        overlay.remove();   // Remove the overlay if it's clicked
        const body = document.querySelector('body');
        body.classList.remove('fix_body'); // Allow scrolling once the user has closed the image overlay display
        console.log('Hola!');
    }

    // Create a button to close the modal window
    const closeModal = document.createElement('P');
    closeModal.textContent = 'X';
    closeModal.classList.add('close_button');
    closeModal.onclick = function() {
        overlay.remove();
        const body = document.querySelector('body');
        body.classList.remove('fix_body'); // Allow scrolling once the user has closed the image overlay display
    }

    // Add the close button to the overlay
    overlay.appendChild(closeModal);

    // Add overlay to HTML
    const body = document.querySelector('body');
    body.appendChild(overlay);
    body.classList.add('fix_body');
}